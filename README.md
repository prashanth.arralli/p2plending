Description: This repository is for Genesis Hackathon Initial POC. It consists of 3 different projects.
1. Cryptolend-network: This is Hyperledger composer project.
2. p2p-middleware: This is a nodejs express project which acts a middle layer between client and hyperledger composer.
3. p2p-client: React UI project

For details steps for setting up the project follow setup.txt.

Project is hosted on AWS Server , baseurl - https://p2plending-genesis.tk/