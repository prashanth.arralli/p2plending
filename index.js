/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const AbortContract = require('./lib/abort-contract');

module.exports.AbortContract = AbortContract;
module.exports.contracts = [ AbortContract ];
