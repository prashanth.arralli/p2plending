export default [
    {
      loanId : 'DEV730658',
      loanAmount  : '1200000000',
      loanType:'SME',
      loanRate : '8%',
      status: 'ISSUED',
      tenure: '120'
    },
    {
      loanId : 'DEV730659',
      loanAmount : '1200000000',
      loanType:'SME',
      loanRate : '8.5%',
      status: 'ISSUED',
      tenure: '80'
    },
    {
      loanId : 'DEV730660',
      loanType : 'Car Loan',
      loanAmount : '1300000',
      loanRate : '7.57%',
      status: 'CLOSED',
      tenure: '50'
    },
    {
      loanId : 'DEV730661',
      loanType : 'Gold Loan',
      loanAmount : '180000000',
      loanRate : '9%',
      status: 'CLOSED',
      tenure: '150'
    },
    {
      loanId : 'DEV730662',
      loanType : 'Gold Loan',
      loanAmount : '190000000',
      loanRate : '10%',
      status: 'PROCESSING',
      tenure: '180'
    },
    {
      loanId : 'DEV730663',
      loanType : 'SME',
      loanAmount : '150000000',
      loanRate : '8%',
      status: 'PROCESSING',
      tenure: '120'
    },
    {
      loanId : 'DEV730664',
      loanType : 'House Loan',
      loanAmount : '100000000',
      loanRate : '8.99%',
      status: 'DUE',
      tenure: '90'
    }
  ];
  