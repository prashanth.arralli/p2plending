export default [
    {
      id: 'DEV730658',
      loansCount: 120,
      capitalRaised: '1200000000',
      netIRR: '8%',
      processingFee: '1.6%',
      status: 'ISSUED'
    },
    {
      id: 'DEV730659',
      loansCount: 120,
      capitalRaised: '1200000000',
      netIRR: '8.5%',
      processingFee: '1.3%',
      status: 'ISSUED'
    },
    {
      id: 'DEV730660',
      loansCount: 155,
      capitalRaised: '1300000000',
      netIRR: '7.57%',
      processingFee: '1.9%',
      status: 'CLOSED'
    },
    {
      id: 'DEV730661',
      loansCount: 148,
      capitalRaised: '180000000',
      netIRR: '9%',
      processingFee: '1.0%',
      status: 'CLOSED'
    },
    {
      id: 'DEV730662',
      loansCount: 150,
      capitalRaised: '190000000',
      netIRR: '10%',
      processingFee: '0.8%',
      status: 'PROCESSING'
    },
    {
      id: 'DEV730663',
      loansCount: 190,
      capitalRaised: '150000000',
      netIRR: '8%',
      processingFee: '1.6%',
      status: 'PROCESSING'
    },
    {
      id: 'DEV730664',
      loansCount: 100,
      capitalRaised: '100000000',
      netIRR: '8.99%',
      processingFee: '1%',
      status: 'DUE'
    }
  ];
  