import React, { Component } from 'react';

// Externals
import PropTypes from 'prop-types';
import classNames from 'classnames';

// Material helpers
import { withStyles, TextField } from '@material-ui/core';

// Material components
import { Grid,Button } from '@material-ui/core';

import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

// Shared layouts
import { Dashboard as DashboardLayout } from '../../layouts';
import {OrdersTable} from './components';
import {get, post} from '../../services/ApiService';



// shared components
import {
    Portlet,
    PortletHeader,
    PortletLabel,
    PortletContent,
    PortletFooter
  } from '../../components';


// Component styles
const styles = theme => ({
    root: {
        padding: theme.spacing(4)
    },
    item: {
        height: '100%'
    },
    burrow_list: {
        padding: theme.spacing(4),
        flexGrow: 1,
        maxWidth: 752,
        backgroundColor: theme.palette.background.paper,
    },
    button:{
        margin:theme.spacing(1)
    }

});


const riskCategories = [
    {
        value:"",
        label:""
    },
    {
        value:"AA+",
        label:"Category AA+"
    },
    {
        value:"AA-",
        label:"Category AA-"
    },
    {
        value:"BB+",
        label:"Category BB+"
    },
    {
        value:"BB-",
        label:"Category BB-"
    },
]
const issuers = [
    {
        value:"",
        label:""
    },
    {
        value:"5da9ed83c1b5cad24e04950c",
        label:"Muthoot Finance"
    },
    {
        value:"5daacac16480ed590d85dfda",
        label:"Bajaj Finance"
    }
]
const diversifiers = [
    {
        value:"",
        label:""
    },
    {
        value:"Auto",
        label:"Auto"
    },
    {
        value:"Manual",
        label:"Manual"
    }
]
class LendMoney extends Component {
    constructor(props){
        super(props);
        const riskCategories = ['A','B'];
        this.handleSubmit = this.handleSubmit.bind(this) ;
        this.createInvestment = this.createInvestment.bind(this) ;
        this.state = {
            amount:0,
            riskCategories:riskCategories,
            loanType:'',
            loanIssuer:'',
            isAutoDiversified:true,
            riskCategory:'',
            issuer:'',
            diversification:'',
            riskCheckedDetails:{
                checkedA: riskCategories.indexOf('AA+') > -1 ? true:false,
                checkedB: riskCategories.indexOf('AA-') > -1 ? true:false,
                checkedC: riskCategories.indexOf('BB+') > -1 ? true:false,
                checkedD: riskCategories.indexOf('BB-') > -1 ? true:false,
            }
          };
          this.getIssuers();
          
    }
    componentDidMount() {
        this.signal = true;
      }
      async getIssuers() {
        console.log("IN GET ISSUERS")
        try {
          const { res } = await get('api/workflow/loan/issuers');
          console.log("awaiting response")
          this.setState({
            issuers:res
          });
        } catch (error) {
          if (this.signal) {
            this.setState({
              isLoading: false,
              error
            });
          }
        }
      }
    handleRiskChange = name => event => {
        this.setState({ ...this.state, "riskCheckedDetails":{...this.state.riskCheckedDetails,[name]: event.target.checked }});
    };
    handleFieldChange = (field, value) => {
        let newState  = { ...this.state }
        newState[field] = value ;
        this.setState(newState);
    };
    handleChange = e => {
        this.setState({
          riskCategory: e.target.value
        });
      };

      handleIssuerChange = e => {
        this.setState({
          issuer: e.target.value
        });
      };
      handleDiversificationChange = e => {
        this.setState({
          diversification: e.target.value
        });
      };
      async createInvestment(reqBody){
        return await post('/api/workflow/investment',reqBody);
      }
      handleSubmit = ()=>{
          let values = { ...this.state}
          let reqBody = {}
          reqBody.amount = values.amount ;
          reqBody.issuerID = values.issuer ;
          reqBody.riskCategory = values.riskCategory;
          this.createInvestment(reqBody).then(response=>{
              console.log("Investment response = ",response);
              if(response.status !== 200){
                  alert("Something went wrong") ;
              }else{
                  alert("Successfully Invested") ;
              }
          }).catch(err=>{
              alert("Something went wrong") ;
          })
      }

    render() {
        const { classes, className, ...rest } = this.props;
        const {amount,riskCategory, issuer, diversification, riskCheckedDetails} = this.state;
        const rootClassName = classNames(classes.root, className);
        return (
            <DashboardLayout title="DashBoard">
                <Portlet
                    {...rest}
                    className={rootClassName}
                >
                <PortletContent noPadding>
                    
                        
                           
                            {/* <FormControl >
                                <FormLabel component="legend">Risk Category</FormLabel>
                                <FormGroup row>
                                    <FormControlLabel
                                        control={
                                        <Checkbox
                                            checked={riskCheckedDetails.checkedA}
                                            onChange={this.handleRiskChange('checkedA')}
                                            value="checkedB"
                                            color="primary"
                                        />
                                        }
                                        label="A"
                                    />
                                    <FormControlLabel
                                        control={
                                        <Checkbox
                                            checked={riskCheckedDetails.checkedB}
                                            onChange={this.handleRiskChange('checkedB')}
                                            value="checkedB"
                                            color="primary"
                                        />
                                        }
                                        label="B"
                                    />
                                    <FormControlLabel
                                        control={
                                        <Checkbox
                                            checked={riskCheckedDetails.checkedC}
                                            onChange={this.handleRiskChange('checkedC')}
                                            value="checkedC"
                                            color="primary"
                                        />
                                        }
                                        label="C"
                                    />
                                    <FormControlLabel
                                        control={
                                        <Checkbox
                                            checked={riskCheckedDetails.checkedD}
                                            onChange={this.handleRiskChange('checkedD')}
                                            value="checkedD"
                                            color="primary"
                                        />
                                        }
                                        label="D"
                                    />
                                </FormGroup>
                            </FormControl> */}
                        <Grid
                            item
                            lg={8}
                            md={12}
                            xl={9}
                            xs={12}
                            >
                            <form
                            autoComplete="off"
                            noValidate
                        >
                             <div className={classes.field}>
                            <TextField
                              className={classes.textField}
                              label="Amount"
                              helperText="Total Amount investing"
                              margin="dense"
                              onChange={event =>
                                this.handleFieldChange('amount', parseFloat(event.target.value))
                              }
                              type="Number"
                              required
                              value={amount}
                              variant="outlined"
                            />
                            </div>
                            <div className={classes.field}>
                            <TextField
                                className={classes.textField}
                                label="Select Risk Category"
                                margin="dense"
                                onChange={this.handleChange}
                                required
                                select
                                SelectProps={{ native: true }}
                                value={riskCategory}
                                style={{"width":"200px"}}
                                variant="outlined">
                                {riskCategories.map(option => (
                                <option
                                    key={option.value}
                                    value={option.value}
                                >
                                    {option.label}
                                </option>
                                ))}
                            </TextField>
                            </div>
                            <div className={classes.field}>
                            <TextField
                                className={classes.textField}
                                label="Select Issuer"
                                margin="dense"
                                onChange={this.handleIssuerChange}
                                required
                                select
                                SelectProps={{ native: true }}
                                value={issuer}
                                style={{"width":"200px"}}
                                variant="outlined">
                                {issuers.map(option => (
                                <option
                                    key={option.value}
                                    value={option.value}
                                >
                                    {option.label}
                                </option>
                                ))}
                            </TextField>
                            </div>
                            <div className={classes.field}>
                            <TextField
                                className={classes.textField}
                                label="Diversification"
                                margin="dense"
                                onChange={this.handleDiversificationChange}
                                required
                                select
                                SelectProps={{ native: true }}
                                value={diversification}
                                style={{"width":"200px"}}
                                variant="outlined">
                                {diversifiers.map(option => (
                                <option
                                    key={option.value}
                                    value={option.value}
                                >
                                    {option.label}
                                </option>
                                ))}
                            </TextField>
                            </div>
                            </form>
                        </Grid>
                        
                        {/* <Grid
                                color="primary"
                              />
                            }
                            label="Primary"
                          />
                            </FormGroup>
                            
                        </Grid>
                        <Grid
                            item
                            lg={8}
                            md={12}
                            xl={9}
                            xs={12}
                            >
                            <OrdersTable className={classes.item} />
                        </Grid>
                        {/* <Grid
                            item
                            lg={8}
                            md={12}
                            xl={9}
                            xs={12}
                            >
                            <BorrowForm className={classes.item} />
                        </Grid> */}
                </PortletContent>

                <PortletFooter className={classes.portletFooter}>
                <Button
                    color="primary"
                    variant="contained"
                    onClick={this.handleSubmit}
                    style={{"textAlign":"right"}}
                >
                    Add Investment
                </Button>
                </PortletFooter>
                </Portlet>
            </DashboardLayout>
        );
    }
}

LendMoney.propTypes = {
    className: PropTypes.string,
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default withStyles(styles)(LendMoney);
