import React, { Component } from 'react';

// Externals
import classNames from 'classnames';
import PerfectScrollbar from 'react-perfect-scrollbar';
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';

// Material components
import {
  Button,
  CircularProgress,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Checkbox
} from '@material-ui/core';

// Shared services
import {get} from '../../../../services/ApiService';

// Shared components
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletToolbar,
  PortletContent,
} from '../../../../components';

// Component styles
import styles from './styles';



function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

class OrdersTable extends Component {
  signal = false;

  state = {
    isLoading: false,
    isModalLoading:false,
    orders: [],
    ordersTotal: 0,
    selectedOrder:{},
    open:false,
    modal:{
      stake:{
        quantity:'',
        status:''
      }
    }
  };

  

  async getOrders() {
    try {
      this.setState({ isLoading: true });

      const { res } = await get('/api/workflow/request/active');
      if (this.signal) {
        this.setState({
          isLoading: false,
          orders:res.slice()
        });
      }
    } catch (error) {
      if (this.signal) {
        this.setState({
          isLoading: false,
          error
        });
      }
    }
  }

  async getOrder(id) {
    if(id)
    try {
      this.setState({ isModalLoading: true });

      const { res } = await get('/api/workflow/request/'+id);
      if (this.signal) {
        this.setState({
          isModalLoading: false,
          modal:{...res}
        });
      }
    } catch (error) {
      if (this.signal) {
        this.setState({
          isLoaisModalLoadingding: false,
          error
        });
      }
    }
  }

  handleSelectOne = (event, order) => {
    this.setState({ selectedOrder: {...order} });

    this.getOrder(order.requestId);
    this.setState({ isModalLoading: true });

  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ isModalLoading: true });
    this.setState({ open: false });
  };

  componentDidMount() {
    this.signal = true;

    this.getOrders();
  }

  componentWillUnmount() {
    this.signal = false;
  }

  render() {
    const { classes, className } = this.props;
    const { isLoading, orders,selectedOrder,isModalLoading ,modal} = this.state;

    const rootClassName = classNames(classes.root, className);
    const showOrders = !isLoading && orders.length > 0;
    const showOrder = !isModalLoading ;

    return (

      <Portlet className={rootClassName}>
        {/* <PortletHeader noDivider>
          <PortletLabel
            title="Loan Requests"
          />
          <PortletToolbar>
            <Button
              className={classes.newEntryButton}
              color="primary"
              size="small"
              variant="outlined"
            >
              Make Payment
            </Button>
          </PortletToolbar>
        </PortletHeader> */}
        <PerfectScrollbar>
          <PortletContent
            className={classes.portletContent}
            noPadding
          >
            {isLoading && (
              <div className={classes.progressWrapper}>
                <CircularProgress />
              </div>
            )}
            {showOrders && (
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>
                    User Name
                    </TableCell>
                    <TableCell align="left">Amount</TableCell>
                    <TableCell
                      align="left"
                      sortDirection="desc"
                    >EMI
                    </TableCell>
                    <TableCell align="left">% Inerest p.a</TableCell>
                    <TableCell align="left">Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {orders.map((order,index) => (
                    <TableRow
                      className={classes.tableRow}
                      hover
                      key={order.requestId}
                      onClick={this.handleOpen}
                    >
                      <TableCell className={classes.customerCell}>
                      <Checkbox
                      checked={selectedOrder && selectedOrder.requestId ? selectedOrder.requestId === order.requestId : false}
                      color="primary"
                      onChange={event =>
                        this.handleSelectOne(event, order)
                      }
                      value={order}
                      />
                      {"User " + index}</TableCell>
                      <TableCell >
                        {order.amount}
                      </TableCell>
                      <TableCell>
                        {order.installmentAmountPerMonth}
                      </TableCell>
                      <TableCell>
                        {order.interestRate}
                      </TableCell>
                      <TableCell>
                          {order.status}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          </PortletContent>
        </PerfectScrollbar>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>

          { isModalLoading && (
            <div className={classes.progressWrapper}>
              <CircularProgress />
            </div>
          )}
          {showOrder && (
            <div>
            <Typography variant="h6" id="modal-title">
              Details of Borrower
            </Typography>
            <Typography variant="subtitle1" id="simple-modal-description">
              Credit score: 760
            </Typography>
            <Typography variant="subtitle1" id="simple-modal-description">
              Lending score: 0
            </Typography>
            <Typography variant="subtitle1" id="simple-modal-description">
              Borrowing score: 760
            </Typography>
            <Typography variant="h6" id="modal-title">
              Stakes
            </Typography>
            <Typography variant="subtitle1" id="simple-modal-description">
              BitCoin : {modal.stake.quantity}
            </Typography>
            <Typography variant="subtitle1" id="simple-modal-description">
              status: {modal.stake.status}
            </Typography>
            </div>
            )}
          </div>
        </Modal>
      </Portlet>
      
    );
  }
}

OrdersTable.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.object.isRequired,
  onSelect: PropTypes.func
};

export default withStyles(styles)(OrdersTable);
