import React, { Component } from 'react';

// Externals
import classNames from 'classnames';
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';

// Material components
import { Button, TextField } from '@material-ui/core';

// Shared components
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent,
  PortletFooter
} from '../../../../components';

// Component styles
import styles from './styles';
import {get, post} from '../../../../services/ApiService';


class BorrowForm extends Component {
  constructor(props){
    super(props) ;
    this.handleSubmit = this.handleSubmit.bind(this) ;
    this.handleFieldChange = this.handleFieldChange.bind(this) ;
    this.createLoan = this.createLoan.bind(this) ;
    this.state = {
      loanNumber:0,
      loanAmount:0,
      loanType:'',
      riskCategory:'',
      interestRate: 0,
      borrowerName:'',
      borrowerPAN:'',
      borrowerAadhar:'',
      borrowerCreditScore:0,
      installments: 0,
      maturityDate:Date.now(),
      startDate:Date.now(),
      installmentPerMonth:0
    };
  }
  handleChange = e => {
    this.setState({
      state: e.target.value
    });
  };

  handleFieldChange = (field, value) => {
    let newState  = { ...this.state }
    newState[field] = value ;
    this.setState(newState);
  };

  async createLoan(reqForm) {
      this.setState({ isLoading: true });

      return await post('/api/workflow/loan/create',reqForm);
  }

  handleSubmit = () => {
    let values = { ...this.state };

    this.setState({ isModalLoading: true });
    let userDetails = localStorage.getItem('user') ;
    userDetails = JSON.parse(userDetails) ;
    this.createLoan({
      issuerID: userDetails._id,
      loanID:values.loanNumber,
      amount:values.loanAmount,
      loanType:values.loanType,
      riskCategory:values.riskCategory,
      interest: values.interestRate,
      borrower_name:values.borrowerName,
      borrower_pan:values.borrowerPAN,
      borrower_aadhar:values.borrowerAadhar,
      creditScore:values.borrowerCreditScore,
      maturityDate:values.maturityDate,
      startDate:values.startDate,
      installmentPerMonth: values.installmentPerMonth,
      duration: values.installments,
    })
     .then(response => {
        if(response.status !== 200){
          if (this.signal) {
            this.setState({
              isLoading: false
            });
          }
          throw new Error(response.text())     
        }else{
         
          return response ;
        }
        
     })
     .then(data => {
        console.log(data);
        alert("Successfully added loan");
        return data;
     })
     .catch(err => {
       this.setState({
        submitError: JSON.stringify(err)
      });
     })
     .finally(() =>{
      this.setState({
        isLoading: false
      });
     });
};

  render() {
    const { classes, className, ...rest } = this.props;
    const { loanNumber,loanAmount,loanType,riskCategory,
      interestRate,borrowerName,borrowerPAN,borrowerAadhar,
      borrowerCreditScore,installments,maturityDate,startDate,installmentPerMonth} = this.state;

    const rootClassName = classNames(classes.root, className);

    return (
      <Portlet
        {...rest}
        className={rootClassName}
      >
        <PortletHeader>
          <PortletLabel
            title="Add Loan"
          />
        </PortletHeader>
        <PortletContent noPadding>
          <form
            autoComplete="off"
            noValidate
          >
            <div className={classes.field}>
              <TextField
                className={classes.textField}
                label="Loan ID"
                margin="dense"
                required
                onChange={event =>
                  this.handleFieldChange('loanNumber', event.target.value)
                }
                value={loanNumber}
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                helperText="Amount of loan"
                label="Amount"
                onChange={event =>
                  this.handleFieldChange('loanAmount', parseFloat(event.target.value))
                }
                type="Number"
                margin="dense"
                required
                value={loanAmount}
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                label="Interest Rate"
                helperText="Interest rate at which borrower can pay the loan back."
                margin="dense"
                onChange={event =>
                  this.handleFieldChange('interestRate', parseFloat(event.target.value))
                }
                type="Number"
                required
                value={interestRate}
                variant="outlined"
              />      
              <TextField
                className={classes.textField}
                label="No* of Installments"
                helperText="number of installments to clear the loan"
                margin="dense"
                type="Number"
                required
                onChange={event =>
                  this.handleFieldChange('installments', parseFloat(event.target.value))
                }
                value={installments}
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                label="Installment Per Month"
                margin="dense"
                type="Number"
                onChange={event =>
                  this.handleFieldChange('installmentPerMonth', parseFloat(event.target.value))
                }
                value={installmentPerMonth}
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                label="Loan Type"
                margin="dense"
                required
                onChange={event =>
                  this.handleFieldChange('loanType', event.target.value)
                }
                value={loanType}
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                label="Risk Category"
                margin="dense"
                required
                onChange={event =>
                  this.handleFieldChange('riskCategory', event.target.value)
                }
                value={riskCategory}
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                label="Loan Maturity Date"
                margin="dense"
                type="Date"
                onChange={event =>
                  this.handleFieldChange('maturityDate', event.target.value)
                }
                value={maturityDate}
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                label="Loan Issued Date"
                margin="dense"
                type="Date"
                onChange={event =>
                  this.handleFieldChange('startDate', event.target.value)
                }
                value={startDate}
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                label="Borrower Aadhar"
                margin="dense"
                required
                onChange={event =>
                  this.handleFieldChange('borrowerAadhar', event.target.value)
                }
                value={borrowerAadhar}
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                label="Borrower Name"
                margin="dense"
                required
                onChange={event =>
                  this.handleFieldChange('borrowerName', event.target.value)
                }
                value={borrowerName}
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                label="Borrower PAN"
                margin="dense"
                required
                onChange={event =>
                  this.handleFieldChange('borrowerPAN', event.target.value)
                }
                value={borrowerPAN}
                variant="outlined"
              />      
              <TextField
                className={classes.textField}
                label="Borrower Credit Score"
                helperText="Credit Score of the Borrower when applying for loan"
                margin="dense"borrowerPAN
                type="Number"
                required
                onChange={event =>
                  this.handleFieldChange('borrowerCreditScore', parseFloat(event.target.value))
                }
                value={borrowerCreditScore}
                variant="outlined"
              />
            </div>
          </form>
        </PortletContent>
        <PortletFooter className={classes.portletFooter}>
          <Button
            color="primary"
            variant="contained"
            onClick={this.handleSubmit}
          >
            Save details
          </Button>
        </PortletFooter>
      </Portlet>
    );
  }
}

BorrowForm.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(BorrowForm);
