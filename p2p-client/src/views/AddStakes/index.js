import React, { Component } from 'react';

// Externals
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';

// Material components
import { Grid } from '@material-ui/core';

// Shared layouts
import { Dashboard as DashboardLayout } from '../../layouts';
import {StakeForm} from './components';



// Custom components


// Component styles
const styles = theme => ({
    root: {
        padding: theme.spacing(4)
    },
    item: {
        height: '100%'
    },
    burrow_list: {
        padding: theme.spacing(4),
        flexGrow: 1,
        maxWidth: 752,
        backgroundColor: theme.palette.background.paper,
    },
    button:{
        margin:theme.spacing(1)
    }

});


  

class AddStakes extends Component {
    constructor(props){
        super(props);
        this.state = {
            dense: false,   
            activeBurrowList:[{"$class":"org.cryptolend.Request","requestId":"a3229475-97b0-4ef6-9752-ec74b1ee4468","amount":100000,"interestRate":15,"installmentAmountPerMonth":15000,"status":"Active","stake":"resource:org.cryptolend.Stake#9c6a460c-7196-433a-bacb-3179fd34273c","requester":"resource:org.cryptolend.User#5ce16174c1d825fb35773c89"}],
            isBurrowListLoading:false
          };;
    }
  

    render() {
        const { classes } = this.props;
        return (
            <DashboardLayout title="Add Stakes/Collaterals">
                <div className={classes.root}>
                    <Grid
                        container
                        spacing={8}
                    >
                        <Grid
                            item
                            lg={8}
                            md={12}
                            xl={9}
                            xs={12}
                            >
                            <StakeForm className={classes.item} />
                        </Grid>
                    </Grid>
                </div>
            </DashboardLayout>
        );
    }
}

AddStakes.propTypes = {
    className: PropTypes.string,
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default withStyles(styles)(AddStakes);
