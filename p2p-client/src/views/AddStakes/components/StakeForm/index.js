import React, { Component } from 'react';

// Externals
import classNames from 'classnames';
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';

// Material components
import { Button, TextField } from '@material-ui/core';

// Shared components
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent,
  PortletFooter
} from '../../../../components';

// Component styles
import styles from './styles';

import {get, post} from '../../../../services/ApiService';


const stakeTypes = [
  {
    value: 'Bitcoin',
    label: 'Bitcoin'
  },
  {
    value: 'Ethereum',
    label: 'Ethereum'
  }
];

class StakeForm extends Component {
  state = {
    amount: 0,
    txnId: '',
    stakeType: 'Bitcoin'
  };

  handleChange = e => {
    this.setState({
      stakeType: e.target.value
    });
  };


  handleFieldChange = (field, value) => {
    let newState  = { ...this.state }
    newState[field] = value ;
    this.setState(newState);
  };

  async createStake(reqForm) {
    try {
      this.setState({ isLoading: true });

      const { res } = await post('/api/workflow/stake/create',reqForm);
      if (this.signal) {
        this.setState({
          isLoading: false
        });
      }
    } catch (error) {
      if (this.signal) {
        this.setState({
          isLoading: false,
          error
        });
      }
    }
  }

  handleSubmit = () => {
    let values = { ...this.state };

    this.setState({ isLoading: true });

    this.createStake({
      "type":values.stakeType,
      "amount":values.amount,
      "txnId":values.txnId
    })
     .then(response => {
        if(response.status !== 200){
          throw new Error(response.text())     
        }else{
          return response.text();
        }
     })
     .then(data => {
        console.log(data);
     })
     .catch(err => {
       this.setState({
        submitError: JSON.stringify(err)
      });
     })
     .finally(() =>{
      this.setState({
        isLoading: false
      });
     });
};

  render() {
    const { classes, className, ...rest } = this.props;
    const { amount, txnId, stakeType } = this.state;

    const rootClassName = classNames(classes.root, className);

    return (
      <Portlet
        {...rest}
        className={rootClassName}
      >
        <PortletHeader>
          <PortletLabel
            title="Add Stake"
          />
        </PortletHeader>
        <PortletContent noPadding>
          <form
            autoComplete="off"
            noValidate
          >
            <div className={classes.field}>
            <TextField
                className={classes.textField}
                label="Type of Stake"
                margin="dense"
                onChange={this.handleChange}
                required
                select
                SelectProps={{ native: true }}
                value={stakeType}
                variant="outlined">
                {stakeTypes.map(option => (
                  <option
                    key={option.value}
                    value={option.value}
                  >
                    {option.label}
                  </option>
                ))}
              </TextField>
              <TextField
                className={classes.textField}
                helperText="Transfer to this platform address and enter the transaction id in given box"
                label="Platform Wallet Address"
                margin="dense"
                required
                value="a134jkdfbadkfjasdlfkwerqwerdjjadf"
                variant="outlined"
              />
            </div>
            <div className={classes.field}>
            <TextField
                className={classes.textField}
                helperText="Amount of Stake you are putting"
                label="Amount"
                margin="dense"
                required
                type="Number"
                value={amount}
                variant="outlined"
                onChange={event =>
                  this.handleFieldChange('amount', parseFloat(event.target.value))
                }
              />
              <TextField
                className={classes.textField}
                label="Transaction Id"
                helperText="Transaction Id that generated after you transfer stake to above address"
                margin="dense"
                required
                value={txnId}
                variant="outlined"
                onChange={event =>
                  this.handleFieldChange('txnId', event.target.value)
                }
              />
              
            </div>
          </form>
        </PortletContent>
        <PortletFooter className={classes.portletFooter}>
          <Button
            color="primary"
            variant="contained"
            onClick={this.handleSubmit}
          >
            Save details
          </Button>
        </PortletFooter>
      </Portlet>
    );
  }
}

StakeForm.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(StakeForm);
