import React, { Component } from 'react';

// Externals
import classNames from 'classnames';
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';
import {ListItemText,Checkbox,ListItem,List} from '@material-ui/core';

// Material components
import { Button, TextField } from '@material-ui/core';

// Shared components
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent,
  PortletFooter
} from '../../../../components';

// Component styles
import styles from './styles';

import {post,get} from '../../../../services/ApiService';


 const details = localStorage.getItem('details');

class CompanyDetails extends Component {
 details = JSON.parse(details);
  state = {
      companyName:this.details ? this.details.companyName: '',
    companyPAN: this.details ? this.details.companyPAN: '',
    leiNumber: this.details ? this.details.leiNumber: ''
  };


  handleTextChange = name => event => {
    this.setState({ [name]: event.target.value });
  };


  submitCompanyDetails = () => {
      debugger
    let postBody = {companyName:this.state.companyName,companyPAN:this.state.companyPAN,leiNumber:this.state.leiNumber};
    post(`/api/users/5ce13eb2786d395e7c9686ba/additionaldetails`,postBody).then(data => console.log(data))
  }

  render() {
    const { classes, className, ...rest } = this.props;
    const { companyName,companyPAN,leiNumber } = this.state;

    const rootClassName = classNames(classes.root, className);

    return (
      <Portlet
        {...rest}
        className={rootClassName}
      >
        <PortletHeader>
          <PortletLabel
            subtitle="The information can be edited"
            title="Company Details"
          />
        </PortletHeader>
        <PortletContent noPadding>
          <form
            autoComplete="off"
            noValidate
          >
            <div className={classes.field}>
              <TextField
                className={classes.textField}
                helperText="Please specify the Company Name"
                label="Company Name*"
                margin="dense"
                required
                value={companyName}
                variant="outlined"
                onChange={this.handleTextChange('companyName')}
              />
              <TextField
                className={classes.textField}
                helperText="Please specify the Pan Card number"
                label="Company Pan Card No*"
                margin="dense"
                required
                value={companyPAN}
                variant="outlined"
                onChange={this.handleTextChange('companyPAN')}
                />
                <TextField
                className={classes.textField}
                helperText="Please specify the LEI number"
                label="LEI No*"
                margin="dense"
                required
                value={leiNumber}
                variant="outlined"
                onChange={this.handleTextChange('leiNumber')}
                />
            
            </div>
          </form>
        </PortletContent>
        <PortletFooter className={classes.portletFooter}>
          <Button
            color="primary"
            variant="contained"
            onClick={this.submitCompanyDetails}
          >
            Save details
          </Button>
        </PortletFooter>
      </Portlet>
    );
  }
}

CompanyDetails.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CompanyDetails);
