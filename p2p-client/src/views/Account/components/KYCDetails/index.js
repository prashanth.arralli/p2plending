import React, { Component } from 'react';

// Externals
import classNames from 'classnames';
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';
import {ListItemText,Checkbox,ListItem,List} from '@material-ui/core';

// Material components
import { Button, TextField } from '@material-ui/core';

// Shared components
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent,
  PortletFooter
} from '../../../../components';

// Component styles
import styles from './styles';

import {post,get} from '../../../../services/ApiService';


  const options = [{
      'header':"Google Pay",
      'key':"googlepay",
      'value':''
  }
  ];


 const details = localStorage.getItem('details');

class KYCDetails extends Component {
 details = JSON.parse(details);
  state = {
      userId:this.details ? this.details._id: '',
    panId: this.details ? this.details.panId: '',
    paymentAccounts: this.details ? {...this.details.paymentAccounts}:{},
    phone:this.details ? this.details.phone:"",
    activePaymentMethod:this.details ? this.details.activePaymentMethod:"",
    options:options
  };

  handleChange = (e,key) => {
      if (e && e.target)
    this.setState({
        activePaymentMethod: e.target.value
    });
  };

  handleTextChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handlePayment = (e,obj) => {
    let p = this.state.paymentAccounts;
    p = {...p,[obj.key]:e.target.value};
    let options = this.state.options
    .map(o => {
        if(o.key === obj.key){
            return {...o,value:e.target.value}
        }else{
            return {...o}
        }
    })
    this.setState({paymentAccounts:p,options:options});
  };
  submitKyc = () => {
      debugger
    let postBody = {phone:this.state.phone,paymentAccounts:this.state.paymentAccounts,activePaymentMethod:this.state.activePaymentMethod,panId:this.state.panId};
    post(`/api/users/5ce13eb2786d395e7c9686ba/additionaldetails`,postBody).then(data => console.log(data))
  }

  render() {
    const { classes, className, ...rest } = this.props;
    const { panId,options,phone } = this.state;

    const rootClassName = classNames(classes.root, className);

    return (
      <Portlet
        {...rest}
        className={rootClassName}
      >
        <PortletHeader>
          <PortletLabel
            subtitle="The information can be edited"
            title="KYC Details"
          />
        </PortletHeader>
        <PortletContent noPadding>
          <form
            autoComplete="off"
            noValidate
          >
            <div className={classes.field}>
              <TextField
                className={classes.textField}
                helperText="Please specify the Pan Card number"
                label="Pan Card No*"
                margin="dense"
                required
                value={panId}
                variant="outlined"
                onChange={this.handleTextChange('panCard')}
              />
              <TextField
                className={classes.textField}
                helperText="Please specify the Pan Card number"
                label="Phone"
                margin="dense"
                required
                value={phone}
                variant="outlined"
                onChange={this.handleTextChange('phone')}
                />
            <List className={classes.root}>
                {options.map(e => (
                <ListItem key={e.key} role={undefined} dense >
                    <Checkbox
                    checked={this.state.activePaymentMethod === e.key}
                    tabIndex={-1}
                    disableRipple
                    onChange={event => this.handleChange(event,e.key)}
                    />
                    <ListItemText primary={e.header} />
                    <TextField
                        className={classes.textField}
                        label="Details"
                        margin="dense"
                        required
                        value={this.state.paymentAccounts.googlepay}
                        variant="outlined"
                        onChange={event => this.handlePayment(event,e)}
                    />
                </ListItem>
                ))}
            </List>
            </div>
          </form>
        </PortletContent>
        <PortletFooter className={classes.portletFooter}>
          <Button
            color="primary"
            variant="contained"
            onClick={this.submitKyc}
          >
            Save details
          </Button>
        </PortletFooter>
      </Portlet>
    );
  }
}

KYCDetails.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(KYCDetails);
