export { default as AccountDetails } from './AccountDetails/index';
export { default as AccountProfile } from './AccountProfile/index';
export {default as KYCDetails} from './KYCDetails';
export { default as CompanyDetails} from './CompanyDetails';
