import React, { Component } from 'react';

// Externals
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';

// Material components
import { Grid,CircularProgress } from '@material-ui/core';

// Shared layouts
import { Dashboard as DashboardLayout } from '../../layouts';

// Custom components
import {  AccountDetails, KYCDetails, CompanyDetails } from './components/index';

import {get} from '../../services/ApiService';

// Component styles
const styles = theme => ({
  root: {
    padding: theme.spacing(4)
  }
});



class Account extends Component {
  signal =false;
  state = { tabIndex: 0,isLoading:false,details:{'_id':"asdasd"},'role':'investor'};

  async getDetails() {
    try {
      this.setState({ isLoading: true });

      const { res } = await get('/api/users/me');
      localStorage.setItem('details',JSON.stringify(res));
      if (this.signal) {
        this.setState({
          isLoading: false,
          details:{_id:res._id},
          role:res.role
        });
      }
    } catch (error) {
      if (this.signal) {
        this.setState({
          isLoading: false
        });
      }
    }
  }
  componentDidMount() {
    this.signal = true;

    // this.getDetails();
  }

  componentWillUnmount() {
    this.signal = false;
  }

  render() {
    const { classes } = this.props;
    const{isLoading,details} = this.state;
    const showDetails = !isLoading && details._id !== null ;
    const isInvestor = this.state.role === 'investor' ? true :false ;
    console.log(isInvestor,"is investor");

    return (
      <DashboardLayout title="Account">
      {isLoading &&
        (<div className={classes.progressWrapper}>
                <CircularProgress />
              </div> )}
        {showDetails && 
        (<div className={classes.root}>
          <Grid
            container
            spacing={8}
          >
            <Grid
              item
              lg={8}
              md={6}
              xl={8}
              xs={12}
            >
              <AccountDetails />
            </Grid>
            <Grid
              item
              lg={8}
              md={6}
              xl={8}
              xs={12}
            >
            {isInvestor && (<div> <KYCDetails /> </div>)}
            {!isInvestor && (<div> <CompanyDetails /> </div>)}
            </Grid>
          </Grid>
        </div>
        )}
      </DashboardLayout>
    );
  }
}

Account.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Account);
