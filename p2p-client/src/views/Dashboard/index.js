import React, { Component } from 'react';

// Externals
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';

// Material components
import { Grid } from '@material-ui/core';

// Shared layouts
import { Dashboard as DashboardLayout } from '../../layouts';
import {IssuesTable,Budget,Profit,Users,IRR} from './components';
import { InvestedMoney, MTMValue, InterestRR } from '../LendMoney/components';
import LendMoney from '../LendMoney';



// Custom components


// Component styles
const styles = theme => ({
    root: {
        padding: theme.spacing(4)
    },
    item: {
        height: '100%'
    },
    burrow_list: {
        padding: theme.spacing(4),
        flexGrow: 1,
        maxWidth: 752,
        backgroundColor: theme.palette.background.paper,
    },
    button:{
        margin:theme.spacing(1)
    }

});


  

class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            dense: false,   
            activeBurrowList:[{"$class":"org.cryptolend.Request","requestId":"a3229475-97b0-4ef6-9752-ec74b1ee4468","amount":100000,"interestRate":15,"installmentAmountPerMonth":15000,"status":"Active","stake":"resource:org.cryptolend.Stake#9c6a460c-7196-433a-bacb-3179fd34273c","requester":"resource:org.cryptolend.User#5ce16174c1d825fb35773c89"}],
            isBurrowListLoading:false,
          };
        // this.setUser = this.setUser.bind(this) ;
        // this.setUser() ;
    }
    componentDidMount(){
        setTimeout(function(ref){
            if(localStorage.user){
                const user = JSON.parse(localStorage.user);
                ref.setState({"user":user});
            }
        },2000,this)
    }

  

    render() {
        const { classes } = this.props;
        return (
            <DashboardLayout title="Dashboard">
                <div className={classes.root}>
                {this.state.user && this.state.user.role==="issuer" &&
                    <Grid
                        container
                        spacing={8}
                    >
                    <Grid
                        item
                        lg={3}
                        sm={6}
                        xl={3}
                        xs={12}
                    >
                        <Budget className={classes.item} />
                    </Grid>
                    <Grid
                        item
                        lg={3}
                        sm={6}
                        xl={3}
                        xs={12}
                        >
                        <Users className={classes.item} />
                    </Grid>
                    <Grid
                        item
                        lg={3}
                        sm={6}
                        xl={3}
                        xs={12}
                        >
                        <Profit className={classes.item} />
                    </Grid>

                    <Grid
                        item
                        lg={3}
                        sm={6}
                        xl={3}
                        xs={12}
                        >
                        <IRR className={classes.item} />
                    </Grid>
                        
                            <IssuesTable className={classes.item} />
                        
                    </Grid>
        }


        {this.state.user && this.state.user.role==="investor" &&
                    <Grid
                        container
                        spacing={8}
                    >
                    <Grid
                        item
                        lg={3}
                        sm={6}
                        xl={3}
                        xs={12}
                    >
                        <InvestedMoney className={classes.item} />
                    </Grid>
                    <Grid
                        item
                        lg={3}
                        sm={6}
                        xl={3}
                        xs={12}
                        >
                        <MTMValue className={classes.item} />
                    </Grid>
                    <Grid
                        item
                        lg={3}
                        sm={6}
                        xl={3}
                        xs={12}
                        >
                        <InterestRR className={classes.item} />
                    </Grid>
                        <Grid
                            item
                            lg={8}
                            md={12}
                            xl={9}
                            xs={12}
                            >
                            <LendMoney className={classes.item} />
                        </Grid>
                    </Grid>
        }

                </div>
            </DashboardLayout>
        );
    }
}

Dashboard.propTypes = {
    className: PropTypes.string,
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default withStyles(styles)(Dashboard);
