import React, { Component } from 'react';

// Externals
import classNames from 'classnames';
import PerfectScrollbar from 'react-perfect-scrollbar';
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';
import Modal from '@material-ui/core/Modal';


//import data
import issues from '../../../../data/issues'

// Material components
import {
  Button,
  CircularProgress,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';

// Shared services
// import { getOrders } from 'services/order';

// Shared components
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletToolbar,
  PortletContent,
  Status
} from '../../../../components';

import AddIssue from './AddIssue';

// Component styles
import styles from './styles';

const statusColors = {
  delivered: 'success',
  pending: 'info',
  refund: 'danger'
};

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 75 + rand();
  const left = 75 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}


class IssuesTable extends Component {
  signal = false;

  state = {
    isLoading: false,
    limit: 10,
    orders: [],
    ordersTotal: 0,
    open:false
  };

  async getOrders(limit) {
    try {
      this.setState({ isLoading: true });

      const { orders, ordersTotal } = {
        orders: issues,
        ordersTotal: issues.length
      };

      if (this.signal) {
        this.setState({
          isLoading: false,
          orders,
          ordersTotal
        });
      }
    } catch (error) {
      if (this.signal) {
        this.setState({
          isLoading: false,
          error
        });
      }
    }
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = (event) => {
    debugger;
    this.setState({ open: false });
  };

  componentDidMount() {
    this.signal = true;

    const { limit } = this.state;

    this.getOrders(limit);
  }

  componentWillUnmount() {
    this.signal = false;
  }

  render() {
    const { classes, className } = this.props;
    const { isLoading, orders, ordersTotal } = this.state;

    const rootClassName = classNames(classes.root, className);
    const showOrders = !isLoading && orders.length > 0;

    return (
      <Portlet style={{"width":"100%","maxWidth":"100%"}} className={rootClassName}>
        <PortletHeader noDivider>
          <PortletLabel
            subtitle={`${ordersTotal} in total`}
            title="ISSUE HISTORY"
          />
          <PortletToolbar>
            <Button
              className={classes.newEntryButton}
              color="primary"
              size="small"
              variant="outlined"
              onClick={this.handleOpen}
            >
              New Issue
            </Button>
          </PortletToolbar>
        </PortletHeader>
        <PerfectScrollbar>
          <PortletContent
            className={classes.portletContent}
            noPadding
          >
            {isLoading && (
              <div className={classes.progressWrapper}>
                <CircularProgress />
              </div>
            )}
            {showOrders && (
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Issue ID</TableCell>
                    <TableCell>Created At</TableCell>
                    <TableCell align="left">#Loans</TableCell>
                    <TableCell align="left">Capital Raised</TableCell>
                    <TableCell align="left">Net IRR</TableCell>
                    <TableCell align="left">Processing Fee</TableCell>
                    <TableCell align="left">Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {orders.map(order => (
                    <TableRow
                      className={classes.tableRow}
                      hover
                      key={order.id}
                    >
                      <TableCell>{order.id}</TableCell>

                      <TableCell>
                        {order.createdAt}
                      </TableCell>
                      <TableCell className={classes.customerCell}>
                        {order.loansCount}
                      </TableCell>
                      <TableCell>
                        {order.capitalRaised}
                      </TableCell>
                      <TableCell>
                        {order.netIRR}
                      </TableCell>
                      <TableCell>
                      {order.processingFee}
                      </TableCell>
                      <TableCell>
                        <div className={classes.statusWrapper}>
                          <Status
                            className={classes.status}
                            color={statusColors[order.status]}
                            size="sm"
                          />
                          {order.status}
                        </div>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          </PortletContent>
        </PerfectScrollbar>

        <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={this.state.open}
        onClose={this.handleClose}
        >
        <div style={getModalStyle()} className={classes.paperModal}>
        <AddIssue onModalClose={this.handleClose} open={this.state.open}></AddIssue>
        </div>
      
        </Modal>

      </Portlet>
    );
  }
}

IssuesTable.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(IssuesTable);
