import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

// Views
import Dashboard from './views/Dashboard/index';
import Account from './views/Account';
import Settings from './views/Settings/index';
import SignUp from './views/SignUp';
import SignIn from './views/SignIn';
import LendMoney from './views/LendMoney/index';
import AddLoanRequest from './views/AddLoanRequest/index';
import AddStakes from './views/AddStakes/index';

// import NotFound from './views/NotFound';

function PrivateRoute({ component: Component, ...rest }) {
    return (
      <Route
        {...rest}
        render={props =>
            localStorage.getItem("isAuthenticated") === "true" ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: "/sign-in",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  }

export default class Routes extends Component {
    render() {
        return (
            <Switch>
                <Redirect
                    exact
                    from="/"
                    to="/dashboard"
                />
                <PrivateRoute
                    component={Dashboard}
                    exact
                    path="/dashboard"
                />
                <PrivateRoute
                    component={LendMoney}
                    exact
                    path="/lendmoney"
                />
                <PrivateRoute
                    component={AddLoanRequest}
                    exact
                    path="/addloan"
                />
                <PrivateRoute
                    component={AddStakes}
                    exact
                    path="/addstake"
                />
                <PrivateRoute
                    component={Account}
                    exact
                    path="/account"
                />
                <PrivateRoute
                    component={Settings}
                    exact
                    path="/settings"
                />
                <Route
                    component={SignIn}
                    exact
                    path="/sign-in"
                />
                <Route
                    component={SignUp}
                    exact
                    path="/sign-up"
                />
                {/*<Route*/}
                    {/*component={NotFound}*/}
                    {/*exact*/}
                    {/*path="/not-found"*/}
                {/*/>*/}
                <Redirect to="/not-found" />
            </Switch>
        );
    }
}
