'use strict' ;

const NS = 'org.cryptolend' ;

/**
* Transfer Currency to the final receiver
* @param {org.cryptolend.TransferToReceiver} tx - The transferTuna transaction
* @transaction
*/
async function transferToReceiver(tx) {
    const currencyRegister = await getAssetRegistry(NS+ '.FiatCurrency') ;

    const receiverRegistry = await getParticipantRegistry(NS+ '.User') ;

    const currency = await currencyRegister.get(tx.currency.getIdentifier()) ;

    if (!currency){
        throw new Error(`Currency txn with id ${tx.currency.getIdentifier()} does not exist`);
    }

    const receiverId = tx.receiver.getIdentifier() ;

    const receiver = await receiverRegistry.get(receiverId) ;

    if (!receiver) {
        throw new Error(`Receiver with id ${receiverId} does not exist`) ;
        //TODO: Initiate the repayment to the sender
    }

    tx.currency.owner = tx.receiver ;

    await currencyRegister.update(tx.currency)

    //Create an event of transfer
    let transferEvent = getFactory().newEvent(NS, 'SentToReceiver')

    transferEvent.currencyId = tx.currency.currencyId ;
    transferEvent.receiverId = tx.receiver.id ;

    emit(transferEvent)
}

/**
* Transfer Currency to the final receiver
* @param {org.hyperledger.composer.system.AddAsset} tx - The transferTuna transaction
* @transaction
*/
async function addAsset(tx){
    const resource = tx.resources[0] ;
    //Create an event of transfer
    console.log(tx)
    let addAssetEvent = getFactory().newEvent(NS, 'AddedFiat')
    if( tx.targetRegistry.registryId === "org.cryptolend.FiatCurrency"){
        addAssetEvent.currencyId = resource.currencyId
        addAssetEvent.amount = resource.amount
        addAssetEvent.type = resource.type
        addAssetEvent.paymentMethod = resource.paymentMethod
        addAssetEvent.txnId = resource.txnId
        addAssetEvent.request = resource.request
        addAssetEvent.owner = resource.owner
        emit(addAssetEvent)
    }
    
}

/**
* Transfer Currency to the final receiver
* @param {org.cryptolend.InvestmentTxn} tx - The investment transaction
* @transaction
*/
async function investmentTxn(tx) {
    const loanRegister = await getAssetRegistry(NS+ '.Loan') ;
    const loan = await loanRegister.get(tx.loan.getIdentifier()) ;

    if (!loan){
        throw new Error(`Loan txn with id ${tx.loan.getIdentifier()} does not exist`);
    }

    tx.loan.amountInvested = tx.loan.amountInvested + tx.amount ;

    await loanRegister.update(tx.loan)

    //Create an event of transfer
    // let transferEvent = getFactory().newEvent(NS, 'SentToReceiver')

    // transferEvent.currencyId = tx.currency.currencyId ;
    // transferEvent.receiverId = tx.receiver.id ;

    // emit(transferEvent)
}

/**
* Transfer Currency to the final receiver
* @param {org.cryptolend.RepaymentTxn} tx - The investment transaction
* @transaction
*/
async function repaymentTxn(tx) {
    const loanRegister = await getAssetRegistry(NS+ '.Loan') ;
    const loan = await loanRegister.get(tx.loan.getIdentifier()) ;

    if (!loan){
        throw new Error(`Loan txn with id ${tx.loan.getIdentifier()} does not exist`);
    }

    tx.loan.amountCleared = tx.loan.amountCleared + tx.amount ;

    //await loanRegister.update(tx.loan)

    //Create an event of transfer
    // let transferEvent = getFactory().newEvent(NS, 'SentToReceiver')

    // transferEvent.currencyId = tx.currency.currencyId ;
    // transferEvent.receiverId = tx.receiver.id ;

    // emit(transferEvent)
}