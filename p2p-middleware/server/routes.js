/**
 * Main application routes
 */

'use strict';

var path = require('path');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/things', require('./api/thing'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));
  app.use('/api/kyc',require('./api/kyc')) ;
  app.use('/api/workflow',require('./api/transactions')) ;
  app.use('/api/loanpools',require('./api/loan_pools'));
};
