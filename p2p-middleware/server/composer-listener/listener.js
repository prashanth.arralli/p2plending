'use strict' ;
const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const winston = require('winston');
var chalk = require('chalk');
let config = require('../config/default.json');
let participantId = config["event-app"]['participantId'];
let participantPwd = config["event-app"]['participantPwd'];
const LOG = winston.loggers.get('application');
const uuidv4 = require('uuid/v4');

class TransferListener{
    constructor() {
        this.namespace = "org.cryptolend"
        this.bizNetworkConnection = new BusinessNetworkConnection();
        this.CONNECTION_PROFILE_NAME = config["event-app"]['connectionProfile'];
        this.businessNetworkIdentifier = config["event-app"]['businessNetworkIdentifier'];
    }

    init() {

        return this.bizNetworkConnection.connect(this.CONNECTION_PROFILE_NAME)
          .then((result) => {
              this.businessNetworkDefinition = result;
              LOG.info("Connection established with Hyperledger composer.")
              LOG.info(this.businessNetworkDefinition.getIdentifier());
          })
          // and catch any exceptions that are triggered
          .catch(function (error) {
              throw error;
          });

    }

        /** Listen for the sale transaction events
            */
     	async listen(){
            var that = this ;
            this.bizNetworkConnection.on('event',(evt)=>{
              console.log("Event name= ",evt.$type)
              let eventType = evt.$type ;
              
              if( eventType === "AddedFiat"){
                
                this.transferToReceiverTxn(evt).then((result)=>{
                    console.log("Submitted txn") ;
                }).catch(function (error) {
                    throw error;
                });
              }else if(eventType === "SentToReceiver"){
                    //if receiver == lender, calculate borrowing
                        //TODO: CALCULATE borrowing score and update installament
                    //if receiver == borrower
                        //TODO: CALCULATE INSTALLMENTS AND CALCULATE LENDING SCORE FOR THE LENDER
                        //Create entries in installments table in mongodb
              }
            });
        }

        async transferToReceiverTxn(evt){
            //TODO: Call payment gateway to transfer to borrower from platform

            let request = evt.request ;
            console.log("owner =",request.$identifier);
            let requestRegistry = await this.bizNetworkConnection.getAssetRegistry(this.namespace+".Request")
            console.log(" registry = ",requestRegistry.name) ;
            let req =  await requestRegistry.get(evt.request.$identifier);
            console.log("req = ",req)
            let serializer = await this.businessNetworkDefinition.getSerializer();

            let resource = serializer.fromJSON({
                '$class': this.namespace+'.TransferToReceiver',
                'currency': evt.currencyId,
                'receiver': req.requester.$identifier
            });

            return await this.bizNetworkConnection.submitTransaction(resource);
        }

        async investInLoan(loanID, userID, amount){
            let serializer = await this.businessNetworkDefinition.getSerializer();

            let resource = serializer.fromJSON({
                '$class': this.namespace+'.InvestmentTxn',
                'loan': loanID,
                'investor': userID,
                'amount':amount
            });
            return await this.bizNetworkConnection.submitTransaction(resource);
        }
        async addUserAsset(user){
            let userRegistry = await this.bizNetworkConnection.getParticipantRegistry(this.namespace+'.User')
            console.log(" registry = ",userRegistry.name) ;
            let factory = this.businessNetworkDefinition.getFactory();
            //TODO: Are paymentmethods of user needs to be stored in composer ?
            //Current we are storing for the easy access of payment methods during installments and failed txns
            let paymentMethod = factory.newConcept(this.namespace,'PaymentMethod')
            if(user.paymentAccounts){
                
                paymentMethod.googlepay = user.paymentAccounts.googlepay ? user.paymentAccounts.googlepay : "" ; 
                
                paymentMethod.phonepe = user.paymentAccounts.phonepe ? user.paymentAccounts.phonepe : "" ;
            
                paymentMethod.upi = user.paymentAccounts.upi ? user.paymentAccounts.upi : "" ;   
                
            }
            console.log("user id = ",user._id.toString()) ;
            console.log(user) ;
            let userAsset = factory.newResource(this.namespace, 'User', user._id.toString());
            userAsset.email = user.email;
            userAsset.phone = user.phone ;
            userAsset.panId = user.panId;
            userAsset.activePaymentMethod = user.activePaymentMethod ;
            userAsset.paymentMethods = paymentMethod ;
            return userRegistry.add(userAsset);
        }
        async addIssuerAsset(issuerObj){
            let issuerRegistry = await this.bizNetworkConnection.getParticipantRegistry(this.namespace+'.Issuer')
            console.log(" registry = ",issuerRegistry.name) ;
            let factory = this.businessNetworkDefinition.getFactory();
            //TODO: Are paymentmethods of user needs to be stored in composer ?
            //Current we are storing for the easy access of payment methods during installments and failed txns
            let issuerAsset = factory.newResource(this.namespace, 'Issuer', issuerObj.id);
            issuerAsset.name = issuerObj.name;
            return issuerRegistry.add(issuerAsset);
        }
        async addStakeAsset(stake){
            let stakeRegistry = await this.bizNetworkConnection.getAssetRegistry(this.namespace+'.Stake')
            console.log(" registry = ",stakeRegistry.name) ;
            let factory = this.businessNetworkDefinition.getFactory();
            // let userRegistry = await this.bizNetworkConnection.getParticipantRegistry(this.namespace+'.User')
            // let userObj = await userRegistry.get(stake.userId) ;
            let userRelation =factory.newRelationship(this.namespace, 'User', stake.userId)   ;          
            let stakeId = uuidv4() ;
            let stakeAsset = factory.newResource(this.namespace, 'Stake', stakeId);
            console.log("stake id = ", stakeId) ;
            stakeAsset.type = stake.type;
            stakeAsset.quantity = stake.quantity ;
            stakeAsset.status = "FREE";
            stakeAsset.txnId = stake.txnId ;
            //stakeAsset.owner = 'resource:'+this.namespace+'.User#'+stake.userId ;
            stakeAsset.owner = userRelation ;
            console.log("Adding stake")
            return stakeRegistry.add(stakeAsset);
        }
        async listUserUnassignedStakes(userId){
            //let requestRegistry = await this.bizNetworkConnection.getAssetRegistry(this.namespace+".Request")
            let userRes = 'resource:'+this.namespace+'.User#'+userId ;
            return this.bizNetworkConnection.query('getParticipantUnassignedStakes',{'userId':userRes}) ;

        }
        async addRequestAsset(reqObj){
            let requestRegistry = await this.bizNetworkConnection.getAssetRegistry(this.namespace+'.Request')
            console.log(" registry = ",requestRegistry.name) ;
            let factory = this.businessNetworkDefinition.getFactory();
            // let userRegistry = await this.bizNetworkConnection.getParticipantRegistry(this.namespace+'.User')
            // let userObj = await userRegistry.get(request.userId) ;
            let userRelation =factory.newRelationship(this.namespace, 'User', reqObj.userId)   ;  
            let stakeRelation =factory.newRelationship(this.namespace, 'Stake', reqObj.stakeId)   ;        
            let requestId = uuidv4() ;
            let requestAsset = factory.newResource(this.namespace, 'Request', requestId);
            console.log("request id = ", requestId) ;
            requestAsset.amount = reqObj.amount;
            requestAsset.interestRate = reqObj.interestRate ;
            requestAsset.installmentAmountPerMonth = reqObj.installmentAmountPerMonth;
            requestAsset.status = "Active" ;

            //requestAsset.owner = 'resource:'+this.namespace+'.User#'+request.userId ;
            requestAsset.requester = userRelation ;
            requestAsset.stake = stakeRelation ;
            console.log("Adding request")
            return requestRegistry.add(requestAsset);
        }

        async addLoanAsset(loanObj){
            let loanRegistry = await this.bizNetworkConnection.getAssetRegistry(this.namespace+'.Loan')
            console.log(" registry = ",loanRegistry.name) ;
            let factory = this.businessNetworkDefinition.getFactory();
            // let userRegistry = await this.bizNetworkConnection.getParticipantRegistry(this.namespace+'.User')
            // let userObj = await userRegistry.get(request.userId) ;
            let issuerRelation =factory.newRelationship(this.namespace, 'Issuer', loanObj.issuerID)   ;  
            let loanAsset = factory.newResource(this.namespace, 'Loan', loanObj.loanID);
            console.log("loan id = ", loanObj.loanID) ;
            loanAsset.amount = loanObj.amount
            loanAsset.shares = loanObj.shares
            loanAsset.interest = loanObj.interest
            loanAsset.creditScore = loanObj.creditScore
            loanAsset.maturityDate = loanObj.maturityDate
            loanAsset.installmentPerMonth = loanObj.installmentPerMonth
            loanAsset.startDate = loanObj.startDate
            loanAsset.duration = loanObj.duration
            loanAsset.riskCategory = loanObj.riskCategory
            loanAsset.loanType = loanObj.loanType

            //requestAsset.owner = 'resource:'+this.namespace+'.User#'+request.userId ;
            loanAsset.issuer = issuerRelation ;
            console.log("Adding Loan")
            return loanRegistry.add(loanAsset);
        }
        async listAllActiveRequests(){
            //let requestRegistry = await this.bizNetworkConnection.getAssetRegistry(this.namespace+".Request")
            let requests = await this.bizNetworkConnection.query('getAllActiveRequests') ;
            return requests ;

        }

        async listUserRequests(userId){
            let userRes = 'resource:'+this.namespace+'.User#'+userId ;

            let requests = await this.bizNetworkConnection.query('getRequestsByParticipant',{'owner':userRes}) ;
            return requests ;
        }
        async getRequestAssetById(requestId){
            let requestRegistry = await this.bizNetworkConnection.getAssetRegistry(this.namespace+'.Request')
            let reqObj = await requestRegistry.resolve(requestId) ;
            return reqObj ;
        }

        async addLenderCurrency(paymentTxn){
            let fiatRegistry = await this.bizNetworkConnection.getAssetRegistry(this.namespace+'.FiatCurrency')
            console.log(" registry = ",fiatRegistry.name) ;
            let factory = this.businessNetworkDefinition.getFactory();
            // let userRegistry = await this.bizNetworkConnection.getParticipantRegistry(this.namespace+'.User')
            // let userObj = await userRegistry.get(fiat.userId) ;
            let userRelation =factory.newRelationship(this.namespace, 'User', paymentTxn.userId)   ;
            let loanReqRelation =factory.newRelationship(this.namespace, 'Request', paymentTxn.requestId)   ;  
            let fiatId = uuidv4() ;
            let fiatAsset = factory.newResource(this.namespace, 'FiatCurrency', fiatId);
            console.log("fiat id = ", fiatId) ;
            fiatAsset.amount = paymentTxn.amount;
            fiatAsset.type = paymentTxn.type ;
            fiatAsset.paymentMethod = paymentTxn.paymentMethod;
            fiatAsset.txnId = paymentTxn.txnId;
            //fiatAsset.owner = 'resource:'+this.namespace+'.User#'+fiat.userId ;
            fiatAsset.owner = userRelation ;
            fiatAsset.request = loanReqRelation ;
            console.log("Adding fiat")
            return fiatRegistry.add(fiatAsset);
        }

        async getLoansByIssuerAndRisk(issuerID, riskType){
            let loanRes = 'resource:'+this.namespace+'.Issuer#'+issuerID ;
            return this.bizNetworkConnection.query('getLoansByRiskAndIssuer',{'issuerID':loanRes,'risk':riskType}) ;
        }

        async getLoansByIssuer(issuerID){
            let loanRes = 'resource:'+this.namespace+'.Issuer#'+issuerID ;
            return this.bizNetworkConnection.query('getLoansByIssuer',{'issuerID':loanRes}) ;
        }

        async getInvestorsByLoanId(loanId){
            let investmentRes = 'resource:'+this.namespace+'.Loan#'+loanId ;
            return this.bizNetworkConnection.query('getInvestorsByLoanId',{'loanId':investmentRes}) ;
        }
        
        async loanRepayment(loanID, userID, amount){
            let serializer = await this.businessNetworkDefinition.getSerializer();

            let resource = serializer.fromJSON({
                '$class': this.namespace+'.RepaymentTxn',
                'loan': loanID,
                'investor': userID,
                'amount':amount
            });
            return await this.bizNetworkConnection.submitTransaction(resource);
        }

}
module.exports = TransferListener
