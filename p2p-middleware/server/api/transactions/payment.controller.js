var validationError = function(res, err) {
    return res.status(422).json(err);
  };

  exports.createPayment = function(req,res,next){
    let paymentTxn = {} ;
    paymentTxn.userId = req.user._id.toString() ;
    paymentTxn.amount = req.body.amount ;
    paymentTxn.txnId = req.body.txnId ;
    paymentTxn.paymentMethod = req.body.paymentMethod ;
    paymentTxn.type = req.body.type ? req.body.type: "LEND" ;
    paymentTxn.requestId = req.body.requestId ;
    req.app.composerObj.addLenderCurrency(paymentTxn).then((result)=>{
        console.log("All requests = ") ;
        console.log(result) ;
        res.status(200).send(result);
    }).catch((err)=>{
        console.log(err) ;
        validationError(res, err) ;
    }) ; 
}