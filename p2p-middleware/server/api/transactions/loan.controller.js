var User = require('./../user/user.model');
var Borrower = require('./../borrower/borrower.model');
var BorrowerController = require('./../borrower/borrower.controller');

var validationError = function(res, err) {
    return res.status(422).json(err);
  };
const users = ["5daa3a04c9fc9923b8e1970b","5daaf79d32101112b9131640"]
exports.addLoan = function(req,res,next){
    let loan = {} ;
    loan.loanID = req.body.loanID
    loan.issuerID = req.body.issuerID;
    loan.amount = req.body.amount ;
    loan.shares = 100 ;
    loan.interest = req.body.interest ;
    loan.creditScore = req.body.creditScore ;
    loan.maturityDate = req.body.maturityDate;
    loan.installmentPerMonth = req.body.installmentPerMonth;
    loan.startDate = req.body.startDate;
    loan.duration = req.body.duration;//In months
    loan.riskCategory = req.body.riskCategory;
    loan.loanType = req.body.loanType;

    
    //Add borrower details in mongodb and loan details to Fabric
    let borrower = new Borrower() ;
    borrower.name  = req.body.borrower_name;
    borrower.panId = req.body.borrower_pan;
    borrower.aadharId = req.body.borrower_aadhar;
    borrower.issuers = [req.body.issuerID];
    console.log("req issuer = ",req.body.issuerID); 
    console.log("issuers = ",borrower.issuers) ;
    borrower.loans = [req.body.loanID] ;
    console.log("loans = ",borrower.loans);
    borrower.creditScore = req.body.creditScore ;
    req.app.composerObj.addLoanAsset(loan).then((result)=>{
        console.log("All requests = ") ;
        console.log(result) ;
        BorrowerController.createOrUpdate(borrower) ;
        res.status(200).send({"msg":"Successfully added loan"});
    }).catch((err)=>{
        console.log(err) ;
        validationError(res, err) ;
    }) ; 
}

exports.getLoansByIssuerAndRisk = function(req, res, next){
    let issuerID = req.body.issuerID;
    let riskCategory = req.body.riskCategory;
    req.app.composerObj.getLoansByIssuerAndRisk(issuerID,riskCategory).then((result)=>{
        console.log("All requests = ") ;
        console.log(result) ;
        res.status(200).send(result);
    }).catch((err)=>{
        console.log(err) ;
        validationError(res, err) ;
    }) ;
}
exports.getLoansByIssuer = function(req, res, next){
    let issuerID = req.body.issuerID;
    req.app.composerObj.getLoansByIssuer(issuerID).then((result)=>{
        console.log("All requests = ") ;
        console.log(result) ;
        res.status(200).send(result);
    }).catch((err)=>{
        console.log(err) ;
        validationError(res, err) ;
    }) ;
}
exports.getIssuers = function(req, res, next){
    User.find({"role":"issuer"},function(err, result){
        if(err){
            console.log(err)
            validationError(res,err);
        }
        res.status(200).send(result) ;
    })
}

exports.loanRepayments = function(req, res, next){
    let issuerID = req.body.issuerID;
    let payments = req.body.payments ;
    repayments = [] ;
    payments.forEach(payment=>{
        let loanId = payment.loanId ;
        let repayment1 = {};
        repayment1.loanID = loanId;
        repayment1.amount = payment.amount/2 ;
        repayment1.userId = users[0] ;
        let repayment2 = {};
        repayment2.loanID = loanId;
        repayment2.amount = payment.amount/2 ;
        repayment2.userId = users[1] ;

        repayments.push(repayment1);
        repayments.push(repayment2);
    })
    let repaymentTxns = repayments.map(repayment=>{
        req.app.composerObj.loanRepayment(repayment.loanID, repayment.userId, repayment.amount) ;
    })
    Promise.all(repaymentTxns)
    .then(responses =>{
        responses.forEach(
            response => console.log(response)
        )
        let resObj = {} ;
        // resObj.investments = investments ;
        // resObj.numberOfLoans = investments.length ;
        // let invstWeightedInterestSum = 0;
        // investments.forEach(invst=>{
        //     invstWeightedInterestSum = invstWeightedInterestSum + invst.amount*invst.interest;
        // })
        // resObj.avgInterest = invstWeightedInterestSum/invstAmt ;
        res.status(200).send({"msg":"Repayment Done"});
    });

}