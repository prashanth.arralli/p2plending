var User = require('./../user/user.model');

var validationError = function(res, err) {
    return res.status(422).json(err);
  };
exports.createLoanRequest = function(req,res,next){
    let loanReq = {} ;
    loanReq.userId = req.user._id.toString() ;
    loanReq.amount = req.body.amount ;
    loanReq.stakeId = req.body.stakeId ;
    loanReq.interestRate = req.body.interestRate ;
    loanReq.installmentAmountPerMonth = req.body.installmentAmountPerMonth ;
    req.app.composerObj.addRequestAsset(loanReq).then((result)=>{
        console.log("All requests = ") ;
        console.log(result) ;
        res.status(200).send(result);
    }).catch((err)=>{
        console.log(err) ;
        validationError(res, err) ;
    }) ; 
}


exports.getRequestById = function(req,res,next){
    let requestId = req.params.id ;
    req.app.composerObj.getRequestAssetById(requestId).then((result)=>{
        console.log("All requests = ") ;
        console.log(result) ;
        if(result.requester){
            User.findById(result.requester.id, function (err, user) {
                if (err) return next(err);
                if (user) {
                    result.requester.borrowingScore = user.borrowingScore ;
                    result.requester.creditScore = user.creditScore ;
                    result.requester.lendingScore = user.lendingScore ? user.lendingScore : 0 ;
                    
                }
                res.status(200).send(result);
              });   
        }else{
            res.status(200).send(result);
        }
        
    }).catch((err)=>{
        return validationError(res, err) ;
    }) ;
}

exports.listActiveLoanRequests = function(req, res, next){
    req.app.composerObj.listAllActiveRequests().then((result)=>{
        console.log("All requests = ") ;
        console.log(result) ;
        res.status(200).send(result);
    }).catch((err)=>{
        return validationError(res, err) ;
    }) ; 
}

exports.listUserRequests = function(req, res, next){
    let userId = req.user._id.toString() ;
    req.app.composerObj.listUserRequests(userId).then((result)=>{
        console.log("All requests = ") ;
        console.log(result) ;
        res.status(200).send(result);
    }).catch((err)=>{
        return validationError(res, err) ;
    }) ; 
}