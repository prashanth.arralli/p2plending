var User = require('./../user/user.model');

var validationError = function(res, err) {
    return res.status(422).json(err);
  };
const MIN_INVESTMENT = 1000;
exports.addInvestment = function(req,res,next){
    let investment = {} ;
    var issuerID = req.body.issuerID;
    var invstAmt = req.body.amount ;
    //investment.interest = req.body.interest ;
    var riskCategory = req.body.riskCategory;
    var userId = req.user._id.toString() ;
    req.app.composerObj.getLoansByIssuerAndRisk(issuerID,riskCategory).then((result)=>{
        
        let numSmallInvestments = invstAmt/MIN_INVESTMENT ;
        var investments = [] ;
        if(result.length > numSmallInvestments){
            let counter = 0 ;
            while(counter <= numSmallInvestments){
                let investmentObj = {} ;
                investmentObj.loanID = result[counter].loanID ;
                investmentObj.userId = userId ;
                investmentObj.amount = MIN_INVESTMENT ;
                investmentObj.interest = result[counter].interest ;
                investments.push(investmentObj) ;
                counter = counter + 1;
            }
        }else{
            let equalAmountInEachLoan= Math.floor(numSmallInvestments/result.length) ;
            console.log("equal amount = ",equalAmountInEachLoan) ;
            let extraInvestments = numSmallInvestments % result.length ;
            console.log("extra amount = ",extraInvestments) ;
            let counter = 0 ;
            while(counter < result.length){
                let investmentObj = {} ;
                investmentObj.loanID = result[counter].loanID ;
                investmentObj.userId = userId ;
                investmentObj.amount = MIN_INVESTMENT*equalAmountInEachLoan ;
                if(counter < extraInvestments){
                    investmentObj.amount = investmentObj.amount + MIN_INVESTMENT ;
                }
                console.log("amount in each loan = ",investmentObj.amount) ;
                investmentObj.interest = result[counter].interest ;
                investments.push(investmentObj) ;
                counter = counter + 1;
            }
        }
        let investmentTxns = investments.map(invstmnt=>{
            req.app.composerObj.investInLoan(invstmnt.loanID, invstmnt.userId, invstmnt.amount) ;
        })
        Promise.all(investmentTxns)
        .then(responses =>{
            responses.forEach(
                response => console.log(response)
            )
            let resObj = {} ;
            resObj.investments = investments ;
            resObj.numberOfLoans = investments.length ;
            let invstWeightedInterestSum = 0;
            investments.forEach(invst=>{
                invstWeightedInterestSum = invstWeightedInterestSum + invst.amount*invst.interest;
            })
            resObj.avgInterest = invstWeightedInterestSum/invstAmt ;
            res.status(200).send(resObj);
        });
    }).catch((err)=>{
        console.log(err) ;
        validationError(res, err) ;
    }) ; 
}