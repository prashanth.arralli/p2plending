var validationError = function(res, err) {
    return res.status(422).json(err);
  };
exports.createStake = function(req, res, next){
    let stake = {} ;
    stake.userId = req.user._id.toString() ;
    stake.type = req.body.type ? req.body.type : "Bitcoin" ;
    stake.quantity = req.body.amount ;
    stake.txnId = req.body.txnId ;
    req.app.composerObj.addStakeAsset(stake).then((result)=>{
        console.log("All requests = ") ;
        console.log(result) ;
        res.status(200).send(result);
    }).catch((err)=>{
        console.log(err) ;
        validationError(res, err) ;
    }) ; 
}

exports.userUnassignedStakes = function(req, res, next){
    let userId = req.user._id.toString() ;
    req.app.composerObj.listUserUnassignedStakes(userId).then((result)=>{
        console.log("All requests = ") ;
        console.log(result) ;
        res.status(200).send(result);
    }).catch((err)=>{
        console.log(err) ;
        validationError(res, err) ;
    }) ; 
}