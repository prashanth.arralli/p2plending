'use strict';

var express = require('express');
var requestcontroller = require('./request.controller');
var loancontroller = require('./loan.controller');
var stakecontroller = require('./stake.controller');
var paymentcontroller = require('./payment.controller');
var invstcontroller = require('./investment.controller') ;
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/request/active', auth.isAuthenticated(), requestcontroller.listActiveLoanRequests);
router.get('/request/:id', auth.isAuthenticated(), requestcontroller.getRequestById);
router.get('/request', auth.isAuthenticated(), requestcontroller.listUserRequests);
router.post('/request/create', auth.isAuthenticated(), requestcontroller.createLoanRequest);
router.post('/stake/create', auth.isAuthenticated(), stakecontroller.createStake);
router.get('/stake/unassigned', auth.isAuthenticated(), stakecontroller.userUnassignedStakes);
router.post('/payment/create', auth.isAuthenticated(), paymentcontroller.createPayment);
router.post('/loan/create', auth.isAuthenticated(), loancontroller.addLoan);
router.post('/loan/getbyissuerandrisk', auth.isAuthenticated(), loancontroller.getLoansByIssuerAndRisk);
router.post('/loan/getbyissuer', auth.isAuthenticated(), loancontroller.getLoansByIssuer);
router.get('/loan/issuers', auth.isAuthenticated(), loancontroller.getIssuers);
router.post('/investment', auth.isAuthenticated(), invstcontroller.addInvestment);
router.post('/loan/repayments', auth.isAuthenticated(), loancontroller.loanRepayments);
module.exports = router;
