'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var BorrowerSchema = new Schema({
  name: String,
  panId: String,
  aadharId: String,
  creditScore:Number,
  issuers: [],
  loans:[]
});

/**
 * Methods
 */
BorrowerSchema.methods = {
};
BorrowerSchema.options ={
    usePushEach: true
}

module.exports = mongoose.model('Borrower', BorrowerSchema);
