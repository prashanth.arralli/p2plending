'use strict';

var Borrower = require('./borrower.model');
var request = require('request');
var validationError = function(err) {
  console.log(err)
};

/**
 * Creates a new borrower
 */
exports.createOrUpdate = function (borrower) {
    Borrower.findOne({"panId":borrower.panId},function(err, brr){
        if(brr){
            console.log("brr = ",brr)
            if(brr.issuers.indexOf(borrower.issuers[0]) < 0){
                brr.issuers.push(borrower.issuers[0])
            }
            brr.loans.push(borrower.loans[0]) ;
            brr.creditScore = borrower.creditScore;
            Borrower.update({"_id":brr._id},{"$set":{"issuers":brr.issuers,"loans":brr.loans}},function(err, user) {
                if (err) return validationError(err);
                return ;
            });
        }else{
            borrower.save(function(err, user) {
                if (err) return validationError(err);
                return ;
            });
        }
    })
  };