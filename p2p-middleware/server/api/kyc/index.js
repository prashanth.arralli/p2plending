'use strict';

var express = require('express');
var controller = require('./kyc.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();
router.post('/verify', auth.isAuthenticated(), controller.verify);
module.exports = router ;