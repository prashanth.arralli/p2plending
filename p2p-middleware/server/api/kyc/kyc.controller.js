'use strict';

var config = require('../../config/environment');

var validationError = function(res, err) {
  return res.status(422).json(err);
};

/**
 * Mock api for KYC verification
 * restriction: 'admin'
 */
exports.verify = function(req, res) {
    res.status(200).json({"verified":true});
};