'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var IssuerSchema = new Schema({
  name: String,
  panId: String,
  leiNmber: String
});

/**
 * Methods
 */
IssuersSchema.methods = {
};

module.exports = mongoose.model('Issuer', IssuerSchema);
