'use strict';

var LoanPool = require('./pools.model');
var request = require('request');
var validationError = function(err) {
  console.log(err)
};

/**
 * Creates a new loan pool
 */
exports.get = function(req, res, next){
    LoanPool.find(function(err, pools){
        if(err){
            console.log(err);
            res.status(500).send(err);
        }else{
            res.status(200).send(pools);
        }
    })
  };