'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var LoanPoolSchema = new Schema({
    pool_id : String,
    originator : String,
    underwriter : String,
    issuance_date : Date,
    closing_note_balance : Number,
    current_note_balance : Number,
    closing_collateral_balance : Number,
    current_collateral_balance : Number,
    pool_factor : Number,
    loans : Number,
    over_collateral_percentage : Number,
    credit_rating : Number,
    xs_percentage : Number,
    wala : Number,
    wam : Number,
    gwac_percentage : Number,
    asset_class : String,
    reserve_balance : Number
});

/**
 * Methods
 */
LoanPoolSchema.methods = {
};
LoanPoolSchema.options ={
    usePushEach: true
}

module.exports = mongoose.model('LoanPool', LoanPoolSchema);
