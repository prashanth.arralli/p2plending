'use strict';

var User = require('./user.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var request = require('request');
var validationError = function(res, err) {
  return res.status(422).json(err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  User.find({}, '-salt -hashedPassword', function (err, users) {
    if(err) return res.status(500).send(err);
    res.status(200).json(users);
  });
};

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  if(!newUser.role )
    newUser.role = 'user';
  newUser.save(function(err, user) {
    if (err) return validationError(res, err);
    if(newUser.role === "issuer"){
      req.app.composerObj.addIssuerAsset(user).then((result)=>{
          console.log("CREATED PARTICIPANT IN COMPOSER") ;
          console.log(result) ;
      }).catch((err)=>{
          console.log(err) ;
      }) ;
    }
    var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
    res.json({ token: token });
  });
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.status(401).send('Unauthorized');
    res.json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if(err) return res.status(500).send(err);
    return res.status(204).send('No Content');
  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationError(res, err);
        res.status(200).send('OK');
      });
    } else {
      res.status(403).send('Forbidden');
    }
  });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function(err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.status(401).send('Unauthorized');
    res.json(user);
  });
};
exports.update = function(req, res,next){
    var userId = req.user._id ;
    User.findById(userId, function(err, user){
        if(req.body.name){
            user.name = req.body.name ;
        }
        //TODO : ADD validations for all user parameters
        if(req.body.phone){
            user.phone = req.body.phone ;
        }
        //TODO: Add other parameters which needs to updated
        user.save((err)=>{
            if(err)
                return validationError(res, err); 
            res.status(200).send('OK');
        })
    }) ;

}
//TODO: Add separate methods for adding KYC details and adding payment methods
/**This method is to add kyc details and paymenthod details of user
 * Once kyc details are added, a request to verify kyc details are made
 * And a participant is added in the Hyperledge composer
 */
exports.addAdditionalDetails = function(req,res,next){
    var userId = req.user._id ;
    if(!req.body.panId || !req.body.paymentAccounts || !req.body.activePaymentMethod){
        return validationError(res, {"message":"BadRequest- One or more required fields (panId, paymentAccounts, activePaymentMethod ) are missing from request body."} )
    }
    User.findById(userId, function(err, user){
        // if(user.verified){
        //     return validationError(res, {"message":"BadRequest- user kyc details are already added and verified"} )
        // }
        
        user.panId = req.body.panId ;
        
        if(req.body.paymentAccounts.googlepay || req.body.paymentAccounts.phonepe || req.body.paymentAccounts.upi){
            user.paymentAccounts = req.body.paymentAccounts ;
        }else{
            return validationError(res, {"message" : "BadRequest - paymentAccounts is a json object and allowed keys are googlepay, phonepe and upi ."});
        }
        if(user.paymentAccounts[req.body.activePaymentMethod]){
            user.activePaymentMethod = req.body.activePaymentMethod
        }else{
            return validationError(res, {"message" : "BadRequest - activePaymentMethod is not added to paymentAccounts of the user"});
        }
        if(req.body.phone){
            user.phone = req.body.phone ;
        }else{
            return validationError(res, {"message" : "BadRequest - phone is required"});
        }
        
        user.save(function(err) {
            if (err)
                return validationError(res, err);
            if(user.panId && user.paymentAccounts){
                //Make request kyc verifier with user details
                request.post(
                    config.kyc.url,
                    { 
                        headers:{ 
                            'content-type':'application/json',
                            'authorization':req.headers.authorization
                        },
                        json: { panId: user.panId } 
                    },
                    function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            if(body.verified){
                                user.verified = true;
                                //TODO: GET CREDIT SCORE OF USER FROM THIRD PARTY
                                user.creditScore = 760 ;
                                user.borrowingScore = user.creditScore
                                user.save(function(err) {
                                    if (err) 
                                        return validationError(res, err);
                                    
                                    //TODO: Add user to hyperledger composer
                                    req.app.composerObj.addUserAsset(user).then((result)=>{
                                        console.log("CREATED PARTICIPANT IN COMPOSER") ;
                                        console.log(result) ;
                                    }).catch((err)=>{
                                        console.log(err) ;
                                    }) ;
                                    res.status(200).send('OK');
                                });
                            }
                        }else{
                            return validationError(res, error) ;
                        }
                    }
                );
            }else{
                res.status(200).send('OK');
            }
            }) ;

    })
}

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};
