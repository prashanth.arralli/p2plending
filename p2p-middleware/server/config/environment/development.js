'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: process.env.MONGOURI || 'mongodb://localhost/p2p-lending'
  },

  seedDB: true
};
