'use strict' ;
const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const winston = require('winston');
var chalk = require('chalk');
let config = require('config').get('event-app');
let participantId = config.get('participantId');
let participantPwd = config.get('participantPwd');
const LOG = winston.loggers.get('application');

class TransferListener{
    constructor() {

        this.bizNetworkConnection = new BusinessNetworkConnection();
        this.CONNECTION_PROFILE_NAME = config.get('connectionProfile');
        this.businessNetworkIdentifier = config.get('businessNetworkIdentifier');
    }

    init() {

        return this.bizNetworkConnection.connect(this.CONNECTION_PROFILE_NAME)
          .then((result) => {
              this.businessNetworkDefinition = result;
              LOG.info(this.businessNetworkDefinition.getIdentifier());
          })
          // and catch any exceptions that are triggered
          .catch(function (error) {
              throw error;
          });

    }

        /** Listen for the sale transaction events
            */
     	async listen(){
            var that = this ;
            this.bizNetworkConnection.on('event',(evt)=>{
              console.log("Event name= ",evt.$type)
              let eventType = evt.$type ;
              
              if( eventType === "AddedFiat"){
                
                this.transferToReceiverTxn(evt).then((result)=>{
                    console.log("Submitted txn") ;
                }).catch(function (error) {
                    throw error;
                });
              }
            });
        }

        async transferToReceiverTxn(evt){
            let request = evt.request ;
            console.log("owner =",request.$identifier);
            let requestRegistry = await this.bizNetworkConnection.getAssetRegistry("org.cryptolend.Request")
            console.log(" registry = ",requestRegistry.name) ;
            let req =  await requestRegistry.get(evt.request.$identifier);
            console.log("req = ",req)
            let serializer = await this.businessNetworkDefinition.getSerializer();

            let resource = serializer.fromJSON({
                '$class': 'org.cryptolend.TransferToReceiver',
                'currency': evt.currencyId,
                'receiver': req.requester.$identifier
            });

            return await this.bizNetworkConnection.submitTransaction(resource);
        }
}

var lnr = new TransferListener();
lnr.init();
lnr.listen()
